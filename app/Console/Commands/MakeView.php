<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MakeView extends Command
{
    protected $viewsPath = 'resources/views/';

    /**
     * Repectivamente:
     * - A assinatura do comando
     * - O nome que deseja colocar na view
     * - A pasta que deseja criar a view. Essa pasta será criada em resources/views
     * e caso essa opção não seja usado a view será criada na raiz de views
     *
     * @var string
     */
    protected $name = 'make:view {name} {--path=}';

    /**
     * A descrição do comando
     *
     * @var string
     */

    protected $description = 'Cria um novo arquivo Blade';

    /**
     * O Construtor do comando.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Essa função contém tudo o que deve acontecer ao executar o comando
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Verificando se o usuário quer criar a view em uma pasta específica ou na raiz de resources/views
         */
        $path = $this->option('path') ? $this->viewsPath . $this->option('path') : $this->viewsPath;

        /**
         * Verificando se a pasta que o usuário escolheu existe, caso não, cria-se
         */
        if (!file_exists($path)) {
            mkdir($path);
        }

        /**
         * Adicionando a extensão .blade.php no nome escolhido pelo usuário
         */
        $filePath = sprintf('%s/%s.blade.php', $path, $this->argument('name'));

        /**
         * Verificando se a view já existe
         */
        if (!File::exists($filePath)) {

            $subfolders = dirname($filePath);

            if ($subfolders !== $path && !File::exists($subfolders)) {
                File::makeDirectory($subfolders, 0777, true);
            }

            File::put($filePath, '');

            $this->info("View criada com sucesso: {$filePath}");
        } else {
            $this->error('Essa view já existe!');
        }
    }

    /**
     * Função que retorna os argumentos do comando
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'O nome da view'],
        ];
    }

    /**
     * Função que retorna as opções do comando
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['path', null, InputOption::VALUE_OPTIONAL, 'Onde essa view deve ser criada']
        ];
    }
}
