<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakeClass extends GeneratorCommand
{
  /**
     * O nome e a assinatura do comando.
     *
     * @var string
     */
  protected $name = 'make:class';

  /**
     * A descrição do comando.
     *
     * @var string
     */
  protected $description = 'Cria uma nova classe';

  /**
     * O tipo de classe sendo gerada.
     *
     * @var string
     */
  protected $type = 'Class';

  /**
     * Substitui o nome da classe no stub fornecido.
     *
     * @param  string $stub
     * @param  string $name
     * @return string
     */
  protected function replaceClass($stub, $name)
  {
    $stub = parent::replaceClass($stub, $name);
    return str_replace('GenericClass', $this->argument('name'), $stub);
  }

  /**
     * Obtém o arquivo stub.
     *
     * @return string
     */
  protected function getStub()
  {
    return app_path() . '/Console/Commands/Stubs/make-class.stub';
  }

  /**
     * Obtém o namespace padrão para a classe.
     *
     * @param  string $rootNamespace
     * @return string
     */
  protected function getDefaultNamespace($rootNamespace)
  {
    return $rootNamespace . '\Classes';
  }

  /**
     * Obtém os argumentos do comando do console.
     *
     * @return array
     */
  protected function getArguments()
  {
    return [
      ['name', InputArgument::REQUIRED, 'O nome da classe.'],
    ];
  }
}

